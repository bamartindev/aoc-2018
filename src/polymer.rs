// This react is slow.  Too many calculations that are not needed for this type of problem.
// Had an ok approach but poor execution.  Found another person who implemented with using vecdeque
// which seems way more reasonable and makes sense!

/*
fn react(chain: String, starting_index: usize) -> (bool, usize, String) {
    let offset = match starting_index {
        0 => 0,
        _ => starting_index - 1,
    };

    for (i, pair) in chain.chars().skip(offset).collect::<Vec<char>>().windows(2).enumerate() {
        //Not a fan of the to upper followed by to string.
        if pair[0] != pair[1] && pair[0].to_uppercase().to_string() == pair[1].to_uppercase().to_string() {
            let new_chain = format!("{}{}", &chain[..offset+i], &chain[offset+i+2..]);
            return (true, offset + i, new_chain);
        }
    }

    (false, 0, chain)
}
*/

fn invert_case(c: char) -> char {
    if c.is_lowercase() {
        c.to_ascii_uppercase()
    } else {
        c.to_ascii_lowercase()
    }
}

fn react(input: &str) -> String {
    input.trim().chars().fold(String::new(), |mut acc, c| {
        if acc.ends_with(invert_case(c)) {
            acc.pop();
        } else {
            acc.push(c);
        }
        acc
    })
}

fn p1(input: &str) -> usize {
    react(&input).len()
}

fn p2(input: &str) -> usize {
    let letters = "abcdefghijklmnopqrstuvwxyz";
    let first_reaction = react(&input);

    let mut counts = [0usize; 26];

    for (i, letter) in letters.chars().enumerate() {
        counts[i] = react(
            &first_reaction
                .chars()
                .filter(|c| c.to_ascii_lowercase() != letter)
                .collect::<String>(),
        )
        .len();
    }

    *counts.iter().min().unwrap()
}

pub fn run() {
    let raw_input = include_str!("../inputs/polymer.txt");

    println!("Day 5");
    println!("p1: {}", p1(&raw_input));
    println!("p2: {}", p2(&raw_input));
}
