use std::collections::{HashMap, HashSet};
use std::isize;
use std::num::ParseIntError;
use std::str::FromStr;
use std::sync::atomic::{self, AtomicUsize};

static COORD_COUNTER: AtomicUsize = atomic::ATOMIC_USIZE_INIT;
const SAFE_DISTANCE_THRESHOLD: isize = 10_000;

#[derive(Debug)]
struct Coord {
    id: usize,
    x: isize,
    y: isize,
}

impl Coord {
    fn build(x: isize, y: isize) -> Self {
        Coord {
            id: COORD_COUNTER.fetch_add(1, atomic::Ordering::SeqCst),
            x,
            y,
        }
    }
}

impl FromStr for Coord {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let coords: Vec<&str> = s.split(", ").collect();

        let x = coords[0].parse::<isize>()?;
        let y = coords[1].parse::<isize>()?;

        Ok(Coord::build(x, y))
    }
}

#[derive(Debug)]
struct DistanceResult(usize, isize);

fn distance(coord: &Coord, x: isize, y: isize) -> DistanceResult {
    let distance = (x - coord.x).abs() + (y - coord.y).abs();
    DistanceResult(coord.id, distance)
}

fn unsafe_coords(input: &str) -> (usize, usize) {
    let mut coords = input
        .lines()
        .filter_map(|c| Coord::from_str(c).ok())
        .collect::<Vec<Coord>>();

    coords.sort_by(|a, b| a.x.cmp(&b.x));

    let min_x = coords[0].x;
    let max_x = coords[coords.len() - 1].x;

    coords.sort_by(|a, b| a.y.cmp(&b.y));

    let min_y = coords[0].y;
    let max_y = coords[coords.len() - 1].y;

    let mut infinite = HashSet::new();
    let mut counts = HashMap::new();
    let mut safe_zone_count = 0;

    for i in min_x - 1..=max_x + 1 {
        for j in min_y - 1..=max_y + 1 {
            let mut distances = coords
                .iter()
                .map(|c| distance(&c, i, j))
                .collect::<Vec<DistanceResult>>();

            distances.sort_by(|a, b| a.1.cmp(&b.1));

            let sum = distances.iter().fold(0, |acc, c| acc + c.1);

            if sum < SAFE_DISTANCE_THRESHOLD {
                safe_zone_count += 1;
            }

            if distances[0].1 != distances[1].1 {
                *counts.entry(distances[0].0).or_insert(0) += 1;

                if i == min_x - 1 || i == max_x + 1 || j == min_y - 1 || j == max_y + 1 {
                    infinite.insert(distances[0].0);
                }
            }
        }
    }

    for coord in infinite.iter() {
        counts.remove(&coord);
    }

    (*counts.values().max().unwrap(), safe_zone_count)
}

pub fn run() {
    let input = include_str!("../inputs/coords.txt");

    println!("Day 6");
    println!("{:?}", unsafe_coords(&input));
}
