use std::collections::HashSet;

#[derive(Debug, Clone, Copy, PartialEq)]
struct Instruction(char, char);

//fn split_instructions<'a>(current_instruction: &str, )

pub fn run() {
    let input = include_str!("../inputs/sleigh.txt");
    let mut result = Vec::new();

    let instructions = input.lines().map(|l| {
        let parts = l.split(" ").collect::<Vec<&str>>();
        Instruction(parts[1].parse::<char>().unwrap(), parts[7].parse::<char>().unwrap())
    }).collect::<Vec<Instruction>>();

    let mut seen = HashSet::new();
    let mut dependent = HashSet::new();

    for instruction in instructions.iter() {
        seen.insert(instruction.0);
        dependent.insert(instruction.1);
    }

    let mut current_instruction = *seen.difference(&dependent).collect::<HashSet<_>>().drain().next().unwrap();

    result.push(current_instruction);

    let mut completed = HashSet::new();
    let mut instruction_queue = Vec::new();
    let mut processed_instructions_count = 0;

    loop {
        for instruction in instructions.iter() {
            if instruction.0 == current_instruction {
                instruction_queue.push(instruction);
            }
        }

        // process one instruction
        instruction_queue.sort_by(|a, b| {
            a.1.cmp(&b.1)
        });

        println!("{:?}", instruction_queue);

        let i = instruction_queue.remove(0);

        result.push(i.1);
        current_instruction = i.1;

        processed_instructions_count += 1;

        if processed_instructions_count == instructions.len() {
            break;
        }
    }
    
    println!("{:?}", instruction_queue);
    println!("{:?}", instructions);
    println!("{:?}", result);
}