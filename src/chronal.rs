const GRID_SIZE: usize = 300;

fn sum_row(row: &[isize]) -> isize {
    let mut row_sum = 0;
    for i in row.iter() {
        row_sum += i;
    }

    row_sum
}

fn max(grid: &[[isize; 300]; 300], custom_grid_size: usize) -> (usize, usize, isize) {
    // value, x, y
    let mut max_sum = (0, 0, isize::min_value());

    for y in 0..GRID_SIZE - custom_grid_size {
        for x in 0..GRID_SIZE - custom_grid_size {
            let mut grid_sum = 0;
            for row in grid.iter().skip(y).take(custom_grid_size) {
                grid_sum += sum_row(&row[x..x + custom_grid_size]);
            }

            if grid_sum > max_sum.2 {
                max_sum = (x + 1, y + 1, grid_sum);
            }
        }
    }

    max_sum
}

pub fn run() {
    let serial_number = 7672;
    let mut grid = [[0isize; 300]; 300];

    for y in 1..=GRID_SIZE {
        for x in 1..=GRID_SIZE {
            let rack_id = x + 10;
            let mut power_level = (rack_id * y) as isize;
            power_level += serial_number;
            power_level *= rack_id as isize;
            power_level = power_level / 100 % 10;
            power_level -= 5;

            grid[y - 1][x - 1] = power_level;
        }
    }

    let (x, y, _) = max(&grid, 3);
    let mut max_sum = (0, 0, 0, isize::min_value());

    // TODO: rewrite brute force - find more cost effecient way to calculate max!
    for i in 3..=300 {
        let (x, y, size) = max(&grid, i);

        if size > max_sum.3 {
            max_sum = (x, y, i, size);
        }
    }

    println!("{}, {}", x, y);
    println!("{}, {}, {}", max_sum.0, max_sum.1, max_sum.2);
}
