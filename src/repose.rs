// TONS OF CLEANUP TO DO ON THIS FILE D:

use std::cmp::Ordering;
use std::collections::HashMap;

#[derive(Debug, Eq)]
struct Entry {
    date: String,
    time: String,
    log: String,
}

impl Ord for Entry {
    fn cmp(&self, other: &Entry) -> Ordering {
        //self.date.cmp(&other.date)
        (&self.date, &self.time).cmp(&(&other.date, &other.time))
    }
}

impl PartialOrd for Entry {
    fn partial_cmp(&self, other: &Entry) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Entry {
    fn eq(&self, other: &Entry) -> bool {
        //self.date == other.date
        (&self.date, &self.time) == (&other.date, &other.time)
    }
}

impl Entry {
    fn new(date: &str, time: &str, log: &str) -> Self {
        Entry {
            date: date.to_owned(),
            time: time.to_owned(),
            log: log.to_owned(),
        }
    }
}

fn order_inputs(input: &str) -> Vec<Entry> {
    let mut ordered = Vec::new();

    for line in input.lines() {
        let parts = line
            .split(|c| c == '[' || c == ']')
            .filter(|s| s != &"")
            .map(|s| s.trim())
            .collect::<Vec<&str>>();

        let date_time = parts[0].split(' ').collect::<Vec<&str>>();

        // Not a fan of ending up with two vecs - how to fix?
        ordered.push(Entry::new(date_time[0], date_time[1], parts[1]));
    }
    ordered.sort();

    ordered
}

fn p1(entries: &Vec<Entry>) -> usize {
    let mut guards: HashMap<&str, HashMap<String, usize>> = HashMap::new();
    let mut current_guard = "";
    let mut falls = &String::new();

    for entry in entries.iter() {
        let parts = entry.log.split(' ').collect::<Vec<&str>>(); // regex?

        if parts.len() == 2 {
            if parts[0] == "falls" {
                falls = &entry.time;
            } else {
                // record time sleeping from falls to awake - 1
                match guards.get_mut(&current_guard) {
                    Some(h) => {
                        let start = falls
                            .split(':')
                            .filter_map(|x| x.parse::<usize>().ok())
                            .collect::<Vec<usize>>();
                        let end = entry
                            .time
                            .split(':')
                            .filter_map(|x| x.parse::<usize>().ok())
                            .collect::<Vec<usize>>();

                        for i in start[1]..end[1] {
                            *h.entry(i.to_string()).or_insert(0) += 1;
                        }
                    }
                    None => panic!("Didn't get a hashmap back!"),
                }
            }
        } else {
            current_guard = parts[1];
            guards
                .entry(current_guard)
                .or_insert(HashMap::with_capacity(60));
        }
    }

    let mut max = 0;
    let mut max_guard = "";

    for (guard, times) in guards.iter() {
        let sum: usize = times.values().sum();

        if sum > max {
            max = sum;
            max_guard = guard;
        }
    }

    let minute = match guards.get(&max_guard) {
        Some(h) => {
            let mut max_time = 0;
            let mut max_min = "";
            for (min, time) in h.iter() {
                if time > &max_time {
                    max_time = *time;
                    max_min = min;
                }
            }
            max_min.parse::<usize>().unwrap()
        }
        None => panic!("Didn't get hashmap back, AGAIN!"),
    };

    let max_guard_value = max_guard.replace('#', "").parse::<usize>().unwrap();

    max_guard_value * minute
}

fn p2(entries: &Vec<Entry>) -> usize {
    let mut guards: HashMap<&str, HashMap<String, usize>> = HashMap::new();
    let mut current_guard = "";
    let mut falls = &String::new();

    for entry in entries.iter() {
        let parts = entry.log.split(' ').collect::<Vec<&str>>(); // regex?

        if parts.len() == 2 {
            if parts[0] == "falls" {
                falls = &entry.time;
            } else {
                // record time sleeping from falls to awake - 1
                match guards.get_mut(&current_guard) {
                    Some(h) => {
                        let start = falls
                            .split(':')
                            .filter_map(|x| x.parse::<usize>().ok())
                            .collect::<Vec<usize>>();
                        let end = entry
                            .time
                            .split(':')
                            .filter_map(|x| x.parse::<usize>().ok())
                            .collect::<Vec<usize>>();

                        for i in start[1]..end[1] {
                            *h.entry(i.to_string()).or_insert(0) += 1;
                        }
                    }
                    None => panic!("Didn't get a hashmap back!"),
                }
            }
        } else {
            current_guard = parts[1];
            guards
                .entry(current_guard)
                .or_insert(HashMap::with_capacity(60));
        }
    }

    let mut highest = (0, 0);
    let mut highest_guard = "";

    for (guard, times) in guards.iter() {
        for (minute, time) in times.iter() {
            if time > &highest.1 {
                highest = (minute.parse::<usize>().unwrap(), *time);
                highest_guard = guard;
            }
        }
    }

    let highest_guard_value = highest_guard.replace('#', "").parse::<usize>().unwrap();

    highest_guard_value * highest.0
}

pub fn run() {
    let raw_input = include_str!("../inputs/repose.txt");

    let ordered = order_inputs(raw_input);

    println!("Day 4");
    println!("p1: {}", p1(&ordered));
    println!("p2: {}", p2(&ordered));
}
