use std::collections::VecDeque;

const GENERATIONS: usize = 101;

#[derive(Debug)]
struct Note {
    pattern: Vec<char>,
    result: char,
}

impl Note {
    // Doing a build method instead of from_string because its results are string based
    fn build(input: &str) -> Self {
        let parts = input.split(" => ").collect::<Vec<&str>>();
        Note {
            pattern: String::from(parts[0]).chars().collect::<Vec<char>>(),
            result: String::from(parts[1]).chars().collect::<Vec<char>>()[0],
        }
    }
}

#[derive(Debug)]
struct Garden {
    pots: VecDeque<char>,
    zero: isize,
}

fn generate_next_state(current_state: &Garden, notes: &[Note]) -> Garden {
    let mut next_state: VecDeque<char> = VecDeque::with_capacity(current_state.pots.len());
    let mut pots = current_state.pots.clone();
    let mut zero = current_state.zero;

    if pots[0] == '#' || pots[1] == '#' {
        for _ in 0..2 {
            pots.push_front('.');
            zero += 1;
        }
    }

    if pots[pots.len() - 2] == '#' || pots[pots.len() - 1] == '#' {
        for _ in 0..2 {
            pots.push_back('.');
        }
    }

    for (index, _) in pots.iter().enumerate() {
        // for each pot in the list, check if its group of 5 matches any patterns in notes list.
        let mut group = Vec::new();

        // isize and usize casting D:
        for i in index as isize - 2..=index as isize + 2 {
            if i < 0 || i >= pots.len() as isize {
                group.push('.');
            } else {
                group.push(pots[i as usize]);
            }
        }

        let mut next_char = '.';
        for note in notes {
            // match?
            if note.pattern == group {
                next_char = note.result;
            }
        }

        next_state.push_back(next_char);
    }

    Garden {
        pots: next_state,
        zero,
    }
}

fn sum_pots(garden: &Garden) -> isize {
    let mut sum = 0;

    for (index, &pot) in garden.pots.iter().enumerate() {
        if pot == '#' {
            sum += index as isize - garden.zero;
        }
    }

    sum
}

pub fn run() {
    let input = include_str!("../inputs/day12.txt");

    let state = Garden {
        pots: input
            .lines()
            .next()
            .unwrap()
            .split(": ")
            .collect::<Vec<&str>>()[1]
            .chars()
            .collect::<VecDeque<char>>(),
        zero: 0,
    };

    let mut notes = Vec::new();

    for line in input.lines().skip(2) {
        notes.push(Note::build(line));
    }

    let mut next_state = state;

    for iteration in 1..=GENERATIONS {
        next_state = generate_next_state(&next_state, &notes);

        if iteration == 20 {
            println!("p1: {}", sum_pots(&next_state));
        }
    }

    // found that after 101 iterations, its was a constant addition of 59 each generation
    // This multiplies the number of remaining generation with 59, then adds it to the sum found on the 101st generation for p2
    let partial = (50_000_000_000 - GENERATIONS) * 59;
    let large_sum = sum_pots(&next_state) + partial as isize;
    println!("sum: {}", large_sum);
}
