use std::collections::HashSet;

fn p1(input: &Vec<isize>) -> isize {
    let mut sum = 0;

    for freq in input.iter() {
        sum += freq;
    }

    sum
}

fn p2(input: &Vec<isize>) -> isize {
    let mut seen_set = HashSet::new();
    let mut sum = 0;

    for freq in input.iter().cycle() {
        sum += freq;

        if seen_set.insert(sum) == false {
            break;
        }
    }

    sum
}

pub fn run() {
    // Update to use include_str and get lines using the lines() method - saw in another solution.
    let raw_input = include_str!("../inputs/frequency.txt");
    // I want to figure out how to pass a map rather than colleting and creating an iterator in the functions...
    let input: Vec<isize> = raw_input
        .lines()
        .filter_map(|x| x.parse::<isize>().ok())
        .collect();

    println!("Day 1");
    println!("p1: {}", p1(&input));
    println!("p2: {}", p2(&input));
}
