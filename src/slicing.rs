use std::collections::HashMap;
//format: #xxx @ 3,2: 5x4

/*
 * Notes for cleanup:
 * 1. get_coords and get_dimensions pretty much the same thing - can probably create a simple splitting function for those if needed
 * 2. Probably a better way to build collisions.  The way this is tracked could make this less brute force for p2.  Is this possible on a single pass?
 * 3. All of the splits (in build collisions and p2) are ugly.  Probably a better way to do this.
 * 4. Any way to use a struct to pack data together in one pass?
 * 5. The double && derefs are annoying - this seems like something that is avoidable.
 * 6. CLEANUP ALL THE SPLITS!
 *
*/

fn get_coords(values: &str) -> (usize, usize) {
    let coords: Vec<usize> = values
        .split(',')
        .filter_map(|x| x.parse::<usize>().ok())
        .collect();
    (coords[0], coords[1])
}

fn get_dimensions(values: &str) -> (usize, usize) {
    let dimensions: Vec<usize> = values
        .split('x')
        .filter_map(|x| x.trim().parse::<usize>().ok())
        .collect();
    (dimensions[0], dimensions[1])
}

fn build_collisions(input: &str) -> HashMap<String, usize> {
    let mut claims: HashMap<String, usize> = HashMap::new();

    for line in input.lines() {
        // TODO: replace ':' with '' to make this cleaner to split on (only once)
        let parts = line.split(':').collect::<Vec<&str>>();
        let coords = parts[0].split(' ').collect::<Vec<&str>>()[2];
        let (x, y) = get_coords(coords);
        let (w, h) = get_dimensions(parts[1]);

        for i in x..x + w {
            for j in y..y + h {
                let key = format!("{}:{}", i, j);
                *claims.entry(key).or_insert(0) += 1;
            }
        }
    }

    claims
}

fn p1(claims: &HashMap<String, usize>) -> usize {
    claims.values().filter(|&&x| x >= 2).count()
}

fn p2(claims: &HashMap<String, usize>, input: &str) -> String {
    //brute force attempt, think of a better way to do this in future.
    for line in input.lines() {
        let parts = line.split(':').collect::<Vec<&str>>();
        let coords = parts[0].split(' ').collect::<Vec<&str>>()[2];
        let (x, y) = get_coords(coords);
        let (w, h) = get_dimensions(parts[1]);
        let mut intact = true;

        'outer: for i in x..x + w {
            for j in y..y + h {
                let key = format!("{}:{}", i, j);
                match claims.get(&key) {
                    Some(&x) => {
                        if x > 1 {
                            intact = false;
                            break 'outer;
                        }
                    }
                    None => panic!("Error getting key from HashMap"),
                }
            }
        }

        if intact {
            return parts[0].split(' ').collect::<Vec<&str>>()[0].to_owned();
        }
    }

    unreachable!()
}

pub fn run() {
    let input = include_str!("../inputs/slicing.txt");

    let collisions = build_collisions(&input);

    println!("Day 3");
    println!("p1: {}", p1(&collisions));
    println!("p2: {}", p2(&collisions, &input));
}
