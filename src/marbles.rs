use std::collections::HashMap;
use std::collections::VecDeque;

use regex::Regex;

// First pass, used a vec which was way too slow for p2.  Switched to an more friendly data structure
// for lots of inserts / removals.

fn solve(part_one: bool) -> usize {
    let input = "405 players; last marble is worth 71700 points";

    let re = Regex::new(r"^(\d+) players; last marble is worth (\d+) points$").unwrap();
    let caps = re.captures(input).unwrap();

    let players = caps[1].parse::<usize>().unwrap();
    let mut last_value = caps[2].parse::<usize>().unwrap();

    if !part_one {
        last_value *= 100;
    }

    // HashMap instead of vector because most players will probably not score.
    // If they all seem to score, I can swap to vector.
    let mut scores = HashMap::new();
    let mut circle = VecDeque::new();
    circle.push_front(0);

    let mut next_value = 1;

    'outer: loop {
        for i in 1..=players {
            if next_value % 23 != 0 {
                for _ in 0..2 {
                    let temp = circle.pop_front().expect("failed to pop_front");
                    circle.push_back(temp);
                }
                circle.push_front(next_value);
            } else {
                for _ in 0..7 {
                    let temp = circle.pop_back().expect("failed on pop_back");
                    circle.push_front(temp);
                }
                *scores.entry(i).or_insert(0) +=
                    next_value + circle.pop_front().expect("no value to pop_back");
            }

            next_value += 1;

            if next_value == last_value {
                break 'outer;
            }
        }
    }

    *scores.values().max().unwrap()
}

pub fn run() {
    println!("p1: {}", solve(true));
    println!("p2: {}", solve(false));
}
