#[macro_use]
extern crate lazy_static;

// mod coords;
// mod frequency;
// mod inventory;
// mod polymer;
// mod repose;
// mod slicing;
//mod sleigh;
// mod marbles;
// mod memory;
// mod chronal;
// mod stars;
mod day12;

fn main() {
    day12::run();
    // chronal::run();
    // stars::run();
    // marbles::run();
    // memory::run();
    // sleigh::run();
    // println!("=============");
    // coords::run();
    // println!("=============");
    // polymer::run();
    // println!("=============");
    // repose::run();
    // println!("=============");
    // slicing::run();
    // println!("=============");
    // inventory::run();
    // println!("=============");
    // frequency::run();
}
