fn solve(input: &[usize], mut index: usize) -> (usize, usize, usize) {
    let mut sum: usize = 0;
    let mut children = Vec::new();
    let mut total = 0;
    let header = &input[index..index + 2];
    let metadata_count = header[1];

    // base case
    // if num children 0, return metadata values listed summed.
    if header[0] == 0 {
        let sum = input.iter().skip(index + 2).take(metadata_count).sum();
        return (sum, index + 2 + metadata_count, sum);
    }

    // offsetting the index for good at this point to make sure there isn't a leap between children (had an initial bug)
    index += 2;
    for _ in 0..header[0] {
        let (partial, new_index, total) = solve(input, index);
        children.push(total);
        sum += partial;
        index = new_index;
    }

    let child_list = &input[index..index + metadata_count];

    for child in child_list.iter() {
        if *child <= children.len() {
            total += children[*child - 1];
        }
    }

    let meta_sum: usize = input.iter().skip(index).take(metadata_count).sum();

    (sum + meta_sum, index + metadata_count, total)
}

pub fn run() {
    let input = include_str!("../inputs/memory.txt");
    let vals = input
        .split(' ')
        .filter_map(|c| c.parse::<usize>().ok())
        .collect::<Vec<_>>();
    let (sum, _, total) = solve(&vals, 0);

    println!("sum: {}", sum);
    println!("total: {}", total);
}
