use std::cmp;
use std::num::ParseIntError;
use std::str::FromStr;

use std::collections::HashSet;
use std::iter::FromIterator;

use regex::Regex;

#[derive(Debug)]
struct Point {
    x: isize,
    y: isize,
    vx: isize,
    vy: isize,
}

impl FromStr for Point {
    type Err = ParseIntError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^position=<(.+)> velocity=<(.+)>$").unwrap();
        }

        let caps = RE.captures(s).unwrap();

        let position = caps[1].split(',').map(|p| p.trim()).collect::<Vec<&str>>();
        let velocity = caps[2].split(',').map(|p| p.trim()).collect::<Vec<&str>>();

        let x_fromstr = position[0].parse::<isize>()?;
        let y_fromstr = position[1].parse::<isize>()?;

        let vx_fromstr = velocity[0].parse::<isize>()?;
        let vy_fromstr = velocity[1].parse::<isize>()?;

        Ok(Point {
            x: x_fromstr,
            y: y_fromstr,
            vx: vx_fromstr,
            vy: vy_fromstr,
        })
    }
}

impl Point {
    fn tick(&mut self) {
        self.x += self.vx;
        self.y += self.vy;
    }

    fn rewind(&mut self) {
        self.x -= self.vx;
        self.y -= self.vy;
    }
}

fn vertical_min_max(points: &[Point]) -> (isize, isize) {
    points.iter().fold(
        (isize::max_value(), isize::min_value()),
        |(min_y, max_y), p| (cmp::min(min_y, p.y), cmp::max(max_y, p.y)),
    )
}

fn horizontal_min_max(points: &[Point]) -> (isize, isize) {
    points.iter().fold(
        (isize::max_value(), isize::min_value()),
        |(min_x, max_x), p| (cmp::min(min_x, p.x), cmp::max(max_x, p.x)),
    )
}

fn vertical_bound_size(points: &[Point]) -> isize {
    let (min, max) = vertical_min_max(&points);
    max - min
}

fn display(points: &[Point]) {
    // Able to use from_iter by calling `use std::iter::FromIterator;`
    let unique_points: HashSet<(isize, isize)> =
        HashSet::from_iter(points.iter().map(|p| (p.x, p.y)));

    let (min_y, max_y) = vertical_min_max(&points);
    let (min_x, max_x) = horizontal_min_max(&points);

    for y in min_y..=max_y {
        for x in min_x..=max_x {
            if unique_points.contains(&(x, y)) {
                print!("*");
            } else {
                print!(" ");
            }
        }
        println!();
    }
}

pub fn run() {
    let input = include_str!("../inputs/stars.txt");
    let mut points = Vec::new();

    for line in input.lines() {
        let point = Point::from_str(line);
        points.push(point.unwrap());
    }

    let mut smallest_bound = isize::max_value();
    let mut ticks = 0;

    loop {
        let current_bounds = vertical_bound_size(&points);

        if current_bounds > smallest_bound {
            println!("It took {} ticks", ticks - 1);
            points.iter_mut().for_each(|p| p.rewind());
            display(&points);
            break;
        }

        points.iter_mut().for_each(|p| p.tick());

        smallest_bound = current_bounds;
        ticks += 1;
    }
}
