use std::collections::HashMap;

fn p2(input: &str) -> String {
    for (index, current) in input.lines().enumerate() {
        for cmp in input.lines().skip(index + 1) {
            if current
                .chars()
                .zip(cmp.chars())
                .filter(|(a, b)| a != b)
                .count()
                == 1
            {
                return current
                    .chars()
                    .zip(cmp.chars())
                    .filter(|(a, b)| a == b)
                    .map(|(a, _)| a)
                    .collect();
            }
        }
    }

    unreachable!()
}

fn p1(input: &str) -> usize {
    let mut two = 0;
    let mut three = 0;

    for line in input.lines() {
        /* originally did HashMap::new() but can use HashMap::with_capacity(26)
         * since we are working with alphabet
         */
        let mut letters = HashMap::with_capacity(26);

        for ch in line.chars() {
            *letters.entry(ch).or_insert(0) += 1;
        }

        if letters.values().any(|&x| x == 2) {
            two += 1;
        }

        if letters.values().any(|&x| x == 3) {
            three += 1;
        }
    }

    two * three
}

pub fn run() {
    let raw_input = include_str!("../inputs/inventory.txt");

    println!("Day 2");
    println!("p1: {}", p1(raw_input));
    println!("p2: {}", p2(raw_input));
}
