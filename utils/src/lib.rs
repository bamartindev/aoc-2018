use std::fs::File;
use std::io::Read;

pub fn open_file_as_string(path: &str) -> String {
    let mut f = File::open(path).expect("file not found");
    let mut contents = String::new();
    f.read_to_string(&mut contents)
        .expect("something went wrong reading the file");
    contents
}

// Open a file and split it with a delim
pub fn open_file_vector(path: &str, delim: &str) -> Vec<String> {
    let data = open_file_as_string(path);
    let split = data.split(delim);

    split.map(|x| x.to_string()).collect::<Vec<String>>()
}